//shiftator cu 2 pozitii
module shl32(in, out);
	parameter DIM=32;
	input [DIM-1:0] in;
	output [DIM-1:0] out;

	assign out=in<<2;
endmodule
