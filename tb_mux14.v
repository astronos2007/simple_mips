module tb_mux14;
	reg [31:0] in0, in1, in2, in3;
	reg [1:0] sel;

	wire [31:0] out;

	mux13 mux13_DUT(in0, in1, in2, in3, sel, out);

	initial begin
		in0=32'b000;
		in1=32'b001;
		in2=32'b111;
		in3=32'b000110010;
		sel=2'b01;
		#10 sel=2'b10;
		#10 in1=32'b100;
		#10 sel=2'b00;
		#10 sel=2'b11;
		#10 $finish;
	end
endmodule
