//registru pe 32 de biti
module reg32(clk, res, dataIn, dataOut);
	input clk, res;
	input [31:0] dataIn;

	output reg [31:0] dataOut;
	
	always @(posedge clk)
		if (res==1'b1)
			dataOut<=32'b0;
		else
			dataOut<=dataIn;
endmodule
