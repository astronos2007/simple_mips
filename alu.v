//Unitatea Aritmetico-Logica
module ALU(ALUctrl, OpA, OpB, Result, ZERO);
	input [3:0] ALUctrl;
	input [31:0] OpA;
	input [31:0] OpB;
	
	output reg [31:0] Result;
	output reg ZERO;

	always @(ALUctrl or OpA or OpB)
		begin
			case(ALUctrl)
				4'b0000: Result=OpA&OpB; //and
				4'b0001: Result=OpA|OpB; //or
				4'b0010: Result=OpA+OpB; //add
				4'b0110: Result=OpA-OpB; //sub
			endcase
			ZERO<=(Result==32'b0)?1'b1:1'b0;
		end
endmodule
