module tb_sign_extend;
	reg [15:0] in;
	wire [31:0] out;

	sign_extend sign_extend_DUT(in, out);

	initial begin
		in=16'b1001;
		#10 in=16'b1001_0111_0011_1010;
		#10 $finish;
	end
endmodule
