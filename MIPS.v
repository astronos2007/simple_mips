module MIPS(clk, res);
	input clk, res;

	wire [31:0] PCin, PCout; //pentru program counter
	wire [31:0] mux_pc_out; //iesirea multiplexorului ce selecteaza adresa ce va fi citita din memorie

	wire [31:0] MemData; //ceea ce se extrage din memorie

	//wire [27:0] shl_out_1; //iesirea shiftatorului ce intra in MUX ca Jump Address
	//nu este nevoie de acest shiftator pentru ca adresare pe cuvant
	//wire [31:0] shl_out_2; //iesirea shiftatorului ce intra in MUX ca al doilea operand ALU

	wire [31:0] instr; //instructiunea din registru

	//codificarile instructiunii
	`define opcode instr[31:26]
	`define rs instr[25:21]
	`define rt instr[20:16]

	//pentru instructiunile de tip R
	`define rd instr[15:11]
	`define shamt instr[10:6]
	`define funct instr[5:0]

	//pentru instructiunile de tip I
	`define immed instr[15:0]

	wire [31:0] rd1, rd2, wd; //corespunzatoare bancului de registre
	wire [4:0] wr;

	wire [31:0] mux_alu_out_A; //iesirea multiplexorului care da primul operand ALU
	wire [31:0] mux_alu_out_B; //iesirea multiplexorului care da al doilea operand ALU

	wire [31:0] mem_data_out; //iesirea registrului de date din memorie

	//semnale de control
	wire RegDst, RegWrite, ALUSrcA, IRWrite, MemtoReg, MemWrite, MemRead, IorD, PCWrite, PCWriteCond;
	wire [1:0] ALUSrcB, PCSource, ALUOp;
	wire Zero; //semnal Zero ALU

	wire [31:0] A, B; //iesirile din registrii operand A si B

	wire [31:0] ALU_result; //rezultatul dat de ALU
	wire [31:0] ALU_out; //iesirea din registrul ALUOut

	wire [3:0] ALUctrl; //semnalul de control al operatiei facute de ALU

	wire [31:0] sign_extend_out; //iesirea extensorului de semn

	//registrul PC
	pc pc_reg(clk, res, (Zero&PCWriteCond)|PCWrite, PCin, PCout);

	//multiplexorul ce da adresa care va fi accesata din memorie
	mux12 mux_mem(PCout, ALU_out, IorD, mux_pc_out);

	//extensorul de semn
	sign_extend sign_extend_inst(`immed, sign_extend_out);

	//shiftatorul ce calculeaza adresa efectiva a instructiunii (depasament x 4)
	//shl32 shl_2(sign_extend_out, shl_out_2);

	//memoria
	memorie mem_inst(clk, mux_pc_out, B, MemData, MemRead, MemWrite); 

	//registrul cu date din memorie
	reg32 data_reg(clk, res, MemData, mem_data_out);

	//registrul instructiune
	instr_reg instr_reg_inst(clk, res, IRWrite, MemData, instr);

	//multiplexorul ce selecteaza intre care din registrele rt (pentru tip I) si rd (pentru tip R) se va scrie
	mux12 #(5) mux_wr(`rt, `rd, RegDst, wr);

	//multiplexorul ce selecteaza ce date se scriu in registru (rezultatul calculat de ALU sau ceea ce se gaseste in registrul de date)
	mux12 mux_wd(ALU_out, mem_data_out, MemtoReg, wd);

	//bancul de registre
	rf reg_field(clk, res, RegWrite, `rs, `rt, wr, rd1, rd2, wd);

	//registru operand A
	reg32 reg_A(clk, res, rd1, A);

	//registru operand B
	reg32 reg_B(clk, res, rd2, B);

	//multiplexorul care decide sursa primului operand ALU
	mux12 mux_alu1(PCout, A, ALUSrcA, mux_alu_out_A);
	
	//multiplexorul care decide sursa celui de al doilea operand ALU
	mux14 mux_alu2(B, 1, sign_extend_out, sign_extend_out, ALUSrcB, mux_alu_out_B); //PC+1 pentru ca adresare pe cuvant

	//ALU
	ALU ALU_inst(ALUctrl, mux_alu_out_A, mux_alu_out_B, ALU_result, Zero);

	//control ALU
	ALUControl alu_contr(ALUOp, `funct, ALUctrl);

	//registrul ALUOut
	reg32 ALUOut(clk, res, ALU_result, ALU_out);

	//shiftatorul folosit pentru adresa de jump
	//shl32 #(28) shl_1({2'b00, instr[25:0]}, shl_out_1);
	//nu il folosim pentru ca adresare pe cuvant

	//multiplexorul ce controleaza actualizarea lui PC
	//mux13 mux_pc(ALU_result, ALU_out, {PCout[31:28], shl_out_1}, PCSource, PCin);
	//nu asa pentru ca adresare pe cuvant
	mux13 mux_pc(ALU_result, ALU_out, sign_extend_out, PCSource, PCin);

	//controlul caii de date
	control control_inst(clk, res, `opcode, RegDst, RegWrite, ALUSrcA, ALUSrcB, ALUOp, PCSource, IRWrite, MemtoReg, MemWrite, MemRead, IorD, PCWrite, PCWriteCond);

endmodule
