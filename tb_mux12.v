module tb_mux12;
	reg [31:0] in0, in1;
	reg sel;

	wire [31:0] out;

	mux12 mux12_DUT(in0, in1, sel, out);

	initial begin
		in0=32'b000;
		in1=32'b001;
		sel=1'b0;
		#10 sel=1'b1;
		#10 in1=32'b100;
		#10 sel=1'b0;
		#10 $finish;
	end
endmodule
