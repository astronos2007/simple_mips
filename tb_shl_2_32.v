module tb_shl;
	reg [31:0] in;

	wire [31:0] out;

	shl32 shl32_DUT(in, out);

	initial begin
		in = 32'b1;
		#10 in = 32'b10;
		#10 in = 32'b11100;
		#10 in = 32'b1000;
		#10 in = 32'b1010;
		#10 in = 32'b11100;
		#10 $finish;
	end
endmodule
