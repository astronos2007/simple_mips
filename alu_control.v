//control Unitate Aritmetico-Logica
module ALUControl(ALUOp, FnField, ALUCtrl);
	input [1:0] ALUOp;
	input [5:0] FnField;

	output [3:0] ALUCtrl;
	reg [3:0] ALUCtrl;

	always @(ALUOp or FnField)
		casex({ALUOp, FnField})
			8'b00_xxxxxx: ALUCtrl<=4'b0010; //load word
			8'b00_xxxxxx: ALUCtrl<=4'b0010; //store word
			8'b01_xxxxxx: ALUCtrl<=4'b0110; //branch if equal
			8'b10_100000: ALUCtrl<=4'b0010; //add
			8'b10_100010: ALUCtrl<=4'b0110; //sub
			8'b10_100101: ALUCtrl<=4'b0000; //and
			8'b10_101010: ALUCtrl<=4'b0001; //or
		endcase
endmodule
