module tb_MIPS;
	reg clk;
	reg res;

	MIPS MIPS_DUT(clk, res);

	always #5 clk=~clk;
	
	initial begin
		clk=1'b0;
		res=1'b0;

		#10 res=1'b1;
		#10 res=1'b0;

		#5600 $finish;
	end

endmodule
