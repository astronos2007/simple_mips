//mux 1 din 2
module mux12(in0, in1, sel, out);
	parameter DIM=32;
	input sel;

	input [DIM-1:0] in0, in1;

	output [DIM-1:0] out;
		
	assign out=(sel==1'b0)?in0:in1;
endmodule
