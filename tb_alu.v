module tb_ALU;
	reg [2:0] ALUOp;
	reg [31:0] OpA, OpB;

	wire [31:0] Result;
	wire ZERO;

	ALU ALU_DUT(ALUOp, OpA, OpB, Result, ZERO);

	initial begin
		OpA=32'b1001; OpB=32'b10;
		ALUOp=2'b00;

		#10 ALUOp=2'b00;
		#10 ALUOp=2'b01; OpB=32'b1010;
		#10 ALUOp=2'b11;
		#10 ALUOp=2'b10;
		#10 $finish;
	end
endmodule
