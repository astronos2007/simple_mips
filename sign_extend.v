//extensor de semn
module sign_extend(in, out);
	input [15:0] in;
	output [31:0] out;

	assign out=(in[15]==1'b0)?in:{16'b1111111111111111, in};
endmodule
