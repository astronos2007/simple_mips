module tb_pc;
	reg clk, res, wen;
	reg [31:0] AddressIn;

	wire [31:0] AddressOut;

	pc pc_DUT(clk, res, wen, AddressIn, AddressOut);

	always #5 clk=~clk;

	initial begin
		clk=1'b0;
		res=1'b1;
		wen=1'b0;
		AddressIn=32'b10;
		#10 AddressIn=31'h80;
		wen=1'b1;
		#50 $finish;		
	end
endmodule
