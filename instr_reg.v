//registrul de instructiuni
module instr_reg(clk, res, wen, instrIn, instrOut);
	input clk, res, wen;
	input [31:0] instrIn;

	output reg [31:0] instrOut;
	
	always @(posedge clk)
		if (res==1'b1)
			instrOut<=32'b0;
		else if (wen)
			instrOut<=instrIn;
endmodule
