//bancul de registre (Register Field)
module rf(clk, res, wen, rr1, rr2, wr, rd1, rd2, wd);
	input clk, res, wen;
	input [4:0] rr1, rr2, wr;
	input [31:0] wd;
	output [31:0] rd1, rd2;

	reg[31:0] bancRegistre [31:0];
	reg[5:0] i;

	assign rd1=(rr1!=32'b0)?bancRegistre[rr1]:32'b0;
	assign rd2=(rr2!=32'b0)?bancRegistre[rr2]:32'b0;

	always@(posedge clk)
		begin
		//if (res==1'b1)
			//for (i=0; i<32; i=i+1)
				//bancRegistre[i]<=32'b0;
		if ((wen==1'b1) && (wr!=32'b0))
			bancRegistre[wr]<=wd;
		end
endmodule
