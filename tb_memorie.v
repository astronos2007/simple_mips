module tb_memorie;
	reg clk;
	reg [31:0] address;
	reg [31:0] WriteData;

	wire [31:0] MemData;

	reg MemRead;
	reg MemWrite;

	memorie M(clk, address, WriteData, MemData, MemRead, MemWrite);

	always #5 clk=~clk;

	initial begin
		clk=1'b0;
		address=32'b0; WriteData=32'b1111_1111; MemRead=1'b1;
		#10 address=32'b1; MemWrite=1'b1;
		#10 address=32'b10;
		#10 address=32'b11;
		#10 address=32'b110;
		#30 $finish;
	end
endmodule
