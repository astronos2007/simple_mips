//Program Counter
module pc(clk, res, wen, AddressIn, AddressOut);
	input clk, res, wen;
	input [31:0] AddressIn;

	output reg [31:0] AddressOut;
	
	always @(posedge clk)
		if (res==1'b1)
			AddressOut<=32'b01010100; //segmentul de cod incepe de la adresa 0x54
		else if (wen)
			AddressOut<=AddressIn;
endmodule
