module tb_rf;
	reg clk;
	reg res;
	reg wen;

	reg [4:0] rr1, rr2, wr;
	reg [31:0] wd;
	wire [31:0] rd1, rd2;

	rf rf_DUT(clk, res, wen, rr1, rr2, wr, rd1, rd2, wd);

	always #5 clk=~clk;

	initial begin
		clk=1'b0;
		res=1'b0;
		#10 res=1'b1;
		#10 $finish;
	end

endmodule
