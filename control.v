module control(clk, res, Op, RegDst, RegWrite, ALUSrcA, ALUSrcB, ALUOp, PCSource, IRWrite, MemtoReg, MemWrite, MemRead, IorD, PCWrite, PCWriteCond);
	input clk, res;	

	input [5:0] Op;

	output reg RegDst, RegWrite, ALUSrcA, IRWrite, MemtoReg, MemWrite, MemRead, IorD, PCWrite, PCWriteCond;
	output reg [1:0] ALUSrcB, PCSource, ALUOp;

	reg [3:0] stare_curenta, stare_urmatoare;

	//modelare functionare FSM
	always@(posedge clk) begin
		if (res==1'b1)
			stare_curenta<=4'b0000;
		else
			stare_curenta<=stare_urmatoare;
	end

	//modelare graf de fluenta
	always@(Op or stare_curenta) begin
		casex ({stare_curenta, Op})
			//instruction fetch
			10'b0000_xxxxxx: stare_urmatoare=4'b0001; //se trece in starea 1 indiferent de intrare

			//instruction decode/register fetch
			10'b0001_100011: stare_urmatoare=4'b0010; //load word
			10'b0001_101011: stare_urmatoare=4'b0010; //store word
			10'b0001_000000: stare_urmatoare=4'b0110; //R-type
			10'b0001_000100: stare_urmatoare=4'b1000; //branch if equal
			10'b0001_000010: stare_urmatoare=4'b1001; //jump

				//memory address computation
				10'b0010_100011: stare_urmatoare=4'b0011; //load word
				10'b0010_101011: stare_urmatoare=4'b0101; //store word

					//memory access (load word)
					10'b0011_xxxxxx: stare_urmatoare=4'b0100; //se trece in starea 1 indiferent de intrare

						//memory read completion step (load word) - INCHEIERE LW (5 cicli)
						10'b0100_xxxxxx: stare_urmatoare=4'b0000; //se trece in starea initiala indiferent de intrare

					//memory access (store word) - INCHEIERE SW (4 cicli)
					10'b0101_xxxxxx: stare_urmatoare=4'b0000; //se trece in starea initiala indiferent de intrare

				//execution
				10'b0110_xxxxxx: stare_urmatoare=4'b0111; //se trece in starea 7 indiferent de intrare

					//R-Type completion - INCHEIERE R-TYPE (4 cicli)
					10'b0111_xxxxxx: stare_urmatoare=4'b0000;

				//branch completion
				10'b1000_xxxxxx: stare_urmatoare=4'b0000; //se trece in starea initiala indiferent de intrare

				//jump completion
				10'b1001_xxxxxx: stare_urmatoare=4'b0000; //se trece in starea initiala indiferent de intrare
			
			default:
				stare_urmatoare=4'bxxxx;
		endcase
	end
	
	// modelare semnale de control in functie de stare
	always@(stare_curenta) begin
		casex (stare_curenta)
			4'b0000: {RegDst, RegWrite, ALUSrcA, ALUSrcB, PCSource, IRWrite, MemtoReg, MemWrite, MemRead, IorD, PCWrite, PCWriteCond, ALUOp}=16'b00_0_01_00_1_001_010_00;
			4'b0001: {RegDst, RegWrite, ALUSrcA, ALUSrcB, PCSource, IRWrite, MemtoReg, MemWrite, MemRead, IorD, PCWrite, PCWriteCond, ALUOp}=16'b00_0_11_00_0_000_000_00;
			4'b0010: {RegDst, RegWrite, ALUSrcA, ALUSrcB, PCSource, IRWrite, MemtoReg, MemWrite, MemRead, IorD, PCWrite, PCWriteCond, ALUOp}=16'b00_1_10_00_0_000_000_00;
			4'b0011: {RegDst, RegWrite, ALUSrcA, ALUSrcB, PCSource, IRWrite, MemtoReg, MemWrite, MemRead, IorD, PCWrite, PCWriteCond, ALUOp}=16'b00_0_00_00_0_001_100_00;
			4'b0100: {RegDst, RegWrite, ALUSrcA, ALUSrcB, PCSource, IRWrite, MemtoReg, MemWrite, MemRead, IorD, PCWrite, PCWriteCond, ALUOp}=16'b01_0_00_00_0_100_000_00;
			4'b0101: {RegDst, RegWrite, ALUSrcA, ALUSrcB, PCSource, IRWrite, MemtoReg, MemWrite, MemRead, IorD, PCWrite, PCWriteCond, ALUOp}=16'b00_0_00_00_0_010_100_00;
			4'b0110: {RegDst, RegWrite, ALUSrcA, ALUSrcB, PCSource, IRWrite, MemtoReg, MemWrite, MemRead, IorD, PCWrite, PCWriteCond, ALUOp}=16'b00_1_00_00_0_000_000_10;
			4'b0111: {RegDst, RegWrite, ALUSrcA, ALUSrcB, PCSource, IRWrite, MemtoReg, MemWrite, MemRead, IorD, PCWrite, PCWriteCond, ALUOp}=16'b11_0_00_00_0_000_000_00;
			4'b1000: {RegDst, RegWrite, ALUSrcA, ALUSrcB, PCSource, IRWrite, MemtoReg, MemWrite, MemRead, IorD, PCWrite, PCWriteCond, ALUOp}=16'b00_1_00_01_0_000_001_01;
			4'b1001: {RegDst, RegWrite, ALUSrcA, ALUSrcB, PCSource, IRWrite, MemtoReg, MemWrite, MemRead, IorD, PCWrite, PCWriteCond, ALUOp}=16'b00_0_00_10_0_000_010_00;
			default: {RegDst, RegWrite, ALUSrcA, ALUSrcB, PCSource, IRWrite, MemtoReg, MemWrite, MemRead, IorD, PCWrite, PCWriteCond, ALUOp}=16'bxx_x_xx_xx_x_xxx_xxx_xx;
		endcase		
	end
endmodule
