//memoria (date + instructiuni)
module memorie(clk, address, WriteData, MemData, MemRead, MemWrite);
	input clk;
	input MemRead;
	input MemWrite;
	input [31:0] address;
	input [31:0] WriteData;
	output [31:0] MemData;

	reg[31:0] memorie [127:0];

	assign MemData=(MemRead==1'b1)?memorie[address]:32'bx; //citirea este asincrona
	
	always @(posedge clk)
		if (MemWrite==1'b1) //scrierea este sincrona
			memorie[address]<=WriteData;
	
	initial $readmemb("memorie.mem", memorie);
endmodule
