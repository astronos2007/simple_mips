module tb_reg32;
	reg clk, res;
	reg [31:0] AddressIn;

	wire [31:0] AddressOut;

	reg32 reg32_DUT(clk, res, AddressIn, AddressOut);

	always #5 clk=~clk;

	initial begin
		clk=1'b0;
		res=1'b1;
		AddressIn=32'b10;
		#10 AddressIn=31'h80;
		#50 $finish;		
	end
endmodule
